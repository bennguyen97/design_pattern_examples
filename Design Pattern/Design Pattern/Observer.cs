﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Design_Pattern
{
    public interface Observer
    {
        void ReceiveNotice(Object sender);
    }

    public abstract class Youtube
    {
        public Youtube()
        {
        }
        private List<Observer> observers = new List<Observer>();
        
        public void Subcribe(Observer obs)
        {
            observers.Add(obs);
        }
        
        public void Unsubcribe(Observer obs)
        {
            observers.Remove(obs);
        }
        
        public void Notify(Object sender)
        {
            foreach (Observer obs in observers)
                obs.ReceiveNotice(sender);
        }

    }

    public class YoutuberChannel : Youtube
    {
        public YoutuberChannel()
        {
        }
        public string name { set; get;}
        
        public void UploadVideo()
        {
            Notify(this);
        }

    }

    public class Subcriber1 : Observer
    {
        public Subcriber1()
        {
        }
        public string name { set; get; }

        public void ReceiveNotice(Object sender)
        {
            YoutuberChannel channel = (YoutuberChannel)sender;
            Console.WriteLine(this.name+" xem video moi cua kenh "+channel.name+ " ngay.");
        }

    }

    public class Subcriber2 : Observer
    {
        public Subcriber2()
        {
        }
        public string name { set; get; }

        public void ReceiveNotice(Object sender)
        {
            YoutuberChannel channel = (YoutuberChannel)sender;
            Console.WriteLine("Luu video moi cua kenh "+ channel.name + " vao danh sach xem sau cua "+this.name);
        }

    }
}