﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Design_Pattern
{
    class Program
    {
        static void Main(string[] args)
        {
            #region Factory
            /* 
            //string giaxe = Car.CarFactory(Car.CarType.Honda).GetPrice().ToString();
            //string giaxe = Car.CarFactory(Car.CarType.Huyndai).GetPrice().ToString();
            //Console.WriteLine("Gia ban cua xe la {0} trieu dong", giaxe);
            */
            #endregion
            #region            
            //var builder = new FerrariBuilder();
            //var director = new SportsCarBuildDirector(builder);

            //var builder1 = new HondaBuilder();
            //var director1 = new SportsCarBuildDirector(builder1);

            //director.Construct("Ferrari", "488 Spider", "Red", 2);
            //Car xe = builder.GetResult();
            //Console.WriteLine("Thong so xe cua ban:\n Hang sx: {0}\n Mau ma: {1}\n Mau: {2}\n So cua xe: {3}\n", xe.Name, xe.Model, xe.Colour, xe.NumDoors);

            //director.Construct("Honda", "Civic", "Blue", 4);
            //Car xe1 = builder.GetResult();
            //Console.WriteLine("Thong so xe cua ban:\n Hang sx: {0}\n Mau ma: {1}\n Mau: {2}\n So cua xe: {3}", xe1.Name, xe1.Model, xe1.Colour, xe1.NumDoors);
            #endregion
            //HouseFacade facade = new HouseFacade();
            //facade.GetIn();
            //facade.GoOut();

            //YoutuberChannel FapTV = new YoutuberChannel() { name="FapTV"};

            //Subcriber1 Phuoc = new Subcriber1() { name = "Phuoc" };
            //Subcriber2 Linh = new Subcriber2() { name = "Linh"};
            //Subcriber1 Huong = new Subcriber1() { name = "Huong" };

            //FapTV.Subcribe(Phuoc);
            //FapTV.Subcribe(Linh);
            //FapTV.Subcribe(Huong);

            //FapTV.UploadVideo();

            //FapTV.Unsubcribe(Phuoc);

            //FapTV.UploadVideo();

            //Character character = new Character();
            //InputKeyboard keyboard = new InputKeyboard();
            //keyboard.buttonO = new PunchCommand();
            //keyboard.buttonP = new BlockCommand();
            //keyboard.buttonA = new GoLeftCommand();
            //keyboard.buttonD = new GoRightCommand();

            //keyboard.PressButton("O");
            //keyboard.PressButton("P");
            //keyboard.PressButton("A");
            //keyboard.PressButton("D");

            Mobilephone j7prime = new PowerBank( new Case(new Mobilephone()));
            Mobilephone zenfone3max = new MobileCard( new PowerBank( new Case(new Mobilephone())));

            Console.WriteLine("You bought {0}", j7prime.Bought());
            Console.WriteLine("You bought {0}",zenfone3max.Bought());
            Console.ReadKey();
        }
    }
}
