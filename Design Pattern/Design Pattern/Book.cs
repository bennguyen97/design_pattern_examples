﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Design_Pattern
{
    class SimpleBook
    {
        private string author;
        private string title;
        public SimpleBook(string author, string title)
        {
            this.author = author;
            this.title = title;
        }
        public string getAuthor()
        {
            return this.author;
        }
        public string getTitle()
        {
            return this.title;
        }
    }

    class BookAdapter
    {
        private SimpleBook book;
        public BookAdapter(SimpleBook book)
        {
            this.book = book;
        }
        public string getAuthorAndTitle()
        {
            return book.getTitle()+ " written by "+ book.getAuthor();
        }
    }

    class Borrower
    {
        public void FindBook()
        {
            var book = new SimpleBook("Gamma, Helm, Johnson, and Vlissides", "Design Patterns");
            var bookAdapter = new BookAdapter(book);
            Console.WriteLine("Author and Title: {0}", bookAdapter.getAuthorAndTitle());
        }
    }
}
