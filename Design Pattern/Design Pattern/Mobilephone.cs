﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Design_Pattern
{

    public class Mobilephone
    {
        public Mobilephone()
        {
        }

        public virtual string Bought()
        {
            return " a mobile phone";
        }

    }

    public class Case : Mobilephone
    {

        private Mobilephone mobilephone=new Mobilephone();
        
        public Case(Mobilephone mp)
        {
            this.mobilephone = mp;
        }
        
        public override string Bought()
        {
            return mobilephone.Bought()+" and a case ";
        }

    }

    public class MobileCard : Mobilephone
    {

        private Mobilephone mobilephone = new Mobilephone();

        public MobileCard(Mobilephone mp)
        {
            this.mobilephone = mp;
        }
        
        public override string Bought()
        {
            return mobilephone.Bought() + " and a mobile card";
        }

    }


    public class PowerBank : Mobilephone
    {

        private Mobilephone mobilephone = new Mobilephone();

        public PowerBank(Mobilephone mp)
        {
            this.mobilephone = mp;
        }
        
        public override string Bought()
        {
            return mobilephone.Bought() + " and a powerbank";
        }

    }
}
