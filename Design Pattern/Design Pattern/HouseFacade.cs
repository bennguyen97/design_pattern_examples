﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Design_Pattern
{
    class HouseFacade
    {
        private Television TV = new Television();
        private Computer PC = new Computer();
        private AirConditoner airConditoner = new AirConditoner();
        private Door door = new Door();
        public void TurnOff()
        {
            TV.Off();
            PC.Off();
            airConditoner.Off();
            door.Close();
        }

        public void TurnOn()
        {
            TV.On();
            PC.On();
            airConditoner.On();
            door.Close();
        }
    }
    class Television
    {
        public Television() { }
        public void On()
        {
            Console.WriteLine("Television is on");
        }

        public void Off()
        {
            Console.WriteLine("Television is off");
        }
    }

    class Computer
    {
        public Computer() { }
        public void On()
        {
            Console.WriteLine("Computer is on");
        }

        public void Off()
        {
            Console.WriteLine("Computer is off");
        }
    }

    class AirConditoner
    {
        public AirConditoner() { }
        public void On()
        {
            Console.WriteLine("AirConditoner is on");
        }

        public void Off()
        {
            Console.WriteLine("AirConditoner is on");
        }
    }

    class Door
    {
        public Door() { }
        public void Open()
        {
            Console.WriteLine("Door is opened");
        }

        public void Close()
        {
            Console.WriteLine("Door is closed");
        }
    }
    class Owner
    {
        private HouseFacade facade = new HouseFacade();
        public void GoOut()
        { 
            facade.TurnOff();
        }
        public void GetIn()
        {
            facade.TurnOn();
        }     
    }
}
