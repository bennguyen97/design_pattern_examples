﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Design_Pattern
{
    public class Character
    {
            public Character()
            {
            }

            public void Punch()
            {
                Console.WriteLine("Vo si dam.");
            }

            public void Block()
            {
                Console.WriteLine("Vo si dung tay do.");
            }

            public void GoLeft()
            {
                Console.WriteLine("Vo si di sang trai.");
            }
            
            public void GoRight()
            {
                Console.WriteLine("Vo si di sang phai.");
            }
        
    }
    
    public abstract class Command
    {
        
        public Command()
        {
        }
        
        protected Character character = new Character();

        public abstract void execute();

    }

    public class InputKeyboard
    {
        public InputKeyboard()
        {
        }
        
        public Command buttonO;
        public Command buttonP;
        public Command buttonA;
        public Command buttonD;
        public void PressButton(string input)
        {
            if (input == "O") buttonO.execute();
            else if (input == "P") buttonP.execute();
            else if (input == "A") buttonA.execute();
            else if (input == "D") buttonD.execute();
        }

    }
    
    public class BlockCommand : Command
    {
        public BlockCommand()
        {
        }
        public override void execute()
        {
            character.Block();
        }

    }
    
    public class GoLeftCommand : Command
    {
        public GoLeftCommand()
        {
        }

        public override void execute()
        {
            character.GoLeft();
        }

    }

    public class GoRightCommand : Command
    {
        
        public GoRightCommand()
        {
        }

        public override void execute()
        {
            character.GoRight();
        }

    }

    public class PunchCommand : Command
    {
        public PunchCommand()
        {
        }

        public override void execute()
        {
            character.Punch();
        }

    }
}
