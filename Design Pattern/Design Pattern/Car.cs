﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using System.Threading.Tasks;

//namespace Design_Pattern
//{
//    public class Car
//    {
//        public string Name { get; }
//        public string Model { get; }
//        public int NumDoors { get; }
//        public string Colour { get; }

//        public Car(string name, string model, string colour, int numDoors)
//        {
//            Name = name;
//            Model = model;
//            Colour = colour;
//            NumDoors = numDoors;
//        }
//    }

//    public abstract class CarBuilder
//    {
//        public string Name { get; set; }
//        public string Model { get; set; }
//        public string Colour { get; set; }
//        public int NumDoors { get; set; }

//        public abstract Car GetResult();
//    }

//    public class FerrariBuilder : CarBuilder
//    {
//        public override Car GetResult()
//        {
//            return new Car("Ferrari", this.Model, this.Colour, this.NumDoors);
//        }
//    }

//    public class HondaBuilder : CarBuilder
//    {
//        public override Car GetResult()
//        {
//            return new Car(this.Name, this.Model, this.Colour, this.NumDoors);
//        }
//    }

//    public class SportsCarBuildDirector
//    {
//        private CarBuilder _builder;
//        public SportsCarBuildDirector(CarBuilder builder)
//        {
//            _builder = builder;
//        }

//        public void Construct(string name, string model, string colour, int number)
//        {
//            _builder.Name = name;
//            _builder.Model = model;
//            _builder.Colour = colour;
//            _builder.NumDoors = number;
//        }
//    }
//    class Program
//    {
//        static void Main(string[] args)
//        {
//            var builder = new FerrariBuilder();
//            var director = new SportsCarBuildDirector(builder);
//            director.Construct("Ferrari", "488 Spider", "Red", 2);
//            Car xe = builder.GetResult();
//            Console.WriteLine("Thong so xe cua ban:\n Hang sx: {0}\n Mau ma: {1}\n Mau: {2}\n So cua xe: {3}", xe.Name, xe.Model, xe.Colour, xe.NumDoors);

//            var builder1 = new FerrariBuilder();
//            var director1 = new SportsCarBuildDirector(builder1);
//            director1.Construct("Honda", "Civic", "Blue", 4);
//            Car xe1 = builder1.GetResult();
//            Console.WriteLine("Thong so xe cua ban:\n Hang sx: {0}\n Mau ma: {1}\n Mau: {2}\n So cua xe: {3}", xe1.Name, xe1.Model, xe1.Colour, xe1.NumDoors);

//            Console.ReadKey();
//        }
//    }

//}
