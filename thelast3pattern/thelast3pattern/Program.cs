﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace thelast3pattern
{
    #region brigde
    //class Program
    //{
    //    static void Main(string[] args)
    //    {
    //        Shape cir = new Circle(new Blue());
    //        cir.ApplyColor();

    //        Shape sqr = new Square(new Red());
    //        sqr.ApplyColor();

    //        Console.ReadKey();
    //    }
    //}
    //public interface IColor
    //{
    //    void ApplyColor();
    //}

    //public class Blue : IColor
    //{
    //    public Blue()
    //    {
    //    }

    //    public void ApplyColor()
    //    {
    //        Console.WriteLine(" blue.");
    //    }

    //}

    //public class Red : IColor
    //{
    //    public Red()
    //    {
    //    }

    //    public void ApplyColor()
    //    {
    //        Console.WriteLine(" red.");
    //    }

    //}

    //public abstract class Shape
    //{
    //    protected IColor color;

    //    public abstract void ApplyColor();

    //}


    //public class Circle : Shape
    //{
    //    public Circle(IColor color)
    //    {
    //        this.color=color;
    //    }

    //    public override void ApplyColor()
    //    {
    //        Console.Write("Circle filled with");
    //        color.ApplyColor();
    //    }

    //}

    //public class Square : Shape
    //{
    //    public Square(IColor color)
    //    {
    //        this.color = color;
    //    }

    //    public override void ApplyColor()
    //    {
    //        Console.Write("Square filled with");
    //        color.ApplyColor();
    //    }

    //}
    #endregion
    #region Template Method
    //class Program
    //{
    //    static void Main(string[] args)
    //    {
    //        HouseTemplate woodenHouseType = new WoodenHouse();
    //        woodenHouseType.BuildHouse();

    //        HouseTemplate brickHhouseType = new BrickHouse();
    //        brickHhouseType.BuildHouse();

    //        Console.ReadKey();
    //    }
    //}


    //public abstract class HouseTemplate
    //{

    //    public void BuildHouse()
    //    {
    //        BuildWalls();
    //        BuildFloor();
    //        BuildWindows();
    //        BuildFoundation();
    //        Console.WriteLine("House is built.\n***********");
    //    }

    //    private void BuildWindows()
    //    {
    //        Console.WriteLine("Building glass windows");
    //    }

    //    public abstract void BuildWalls();

    //    public abstract void BuildFloor();

    //    private void BuildFoundation()
    //    {
    //        Console.WriteLine("Building foundation with cement,iron rods and sand.");
    //    }

    //}

    //public class WoodenHouse : HouseTemplate
    //{
    //    public override void BuildWalls()
    //    {
    //        Console.WriteLine("Building wooden walls.");
    //    }

    //    public override void BuildFloor()
    //    {
    //        Console.WriteLine("Building wooden floor.");
    //    }

    //}

    //public class BrickHouse : HouseTemplate
    //{     
    //    public override void BuildWalls()
    //    {
    //        Console.WriteLine("Building brick walls.");
    //    }
        
    //    public override void BuildFloor()
    //    {
    //        Console.WriteLine("Building brick floor.");
    //    }

    //}
    #endregion
    #region strategy
    class Program
    {
        static void Main(string[] args)
        {
            ShoppingCart cartOfLinh = new ShoppingCart(150000);
            cartOfLinh.Pay(new CashOnDeliverlyStrategy("Linh"));

            ShoppingCart cartOfLong = new ShoppingCart(600000);
            cartOfLong.Pay(new CreditCardStrategy("Long","2151998","31/12/2019"));
            Console.ReadKey();
        }
    }

    public interface IPaymentStrategy
    {
        void Pay(int amount);
    }

    public class CreditCardStrategy : IPaymentStrategy
    {

        private string name { set; get; }
        private string cardNumber { set; get; }
        private string dateOfExpiry { set; get; }

        public CreditCardStrategy(string name, string cardNumber, string expirydate)
        {
            this.name = name;
            this.cardNumber = cardNumber;
            this.dateOfExpiry = expirydate;
        }

        public void Pay(int amount)
        {
            Console.WriteLine(name + " will pay" + amount + " vnd with credit/debit card");
        }

    }

    public class CashOnDeliverlyStrategy : IPaymentStrategy
    {
        private string name;
        public CashOnDeliverlyStrategy(string name)
        {
            this.name = name;
        }
        
        public void Pay(int amount)
        {
            Console.WriteLine(name + " will pay" + amount + " vnd when goods delivered.");
        }

    }

    public class ShoppingCart
    {
        private int amount;
        public ShoppingCart(int price)
        {
            this.amount = price;
        }
        public void Pay(IPaymentStrategy paymentMethod)
        {
            paymentMethod.Pay(amount);
        }

    }
    #endregion
}
